# Ubuntu VPS setup (basic security steps)

Setting up a VPS can be inexpensive and relatively easy, once you learn a few basics about Linux cli. I found out quickly that as soon as you put a server online, people try knocking on the door.

This document serves as a basic guide for setting up VPS. This may not be suitable for every situation, make sure you understand what your own risks are. This is in no way meant to be a comprhesive security guide. These steps will not make your VPS "hacker proof", but it will help quite a bit.

> Disclaimer: I am not a Linux expert. These steps are presented with my best understanding of Linux at the time and strive to present material in an approachable way for others who are learning. I'm sure there are some "technically" inaccurate explanations or approaches here. *I'm open to feedback.*

**Getting used to the Linux command line is a bit of a requirement.**

## Helpful commands

Linux is a very powerful OS. The more I use it and get used to it, the more I like it. The Linux community is *very* welcoming to newcomers, but there is a very high expectation for the users to **RTFx**.  

| Name | Function |
| ---- | --- |
| `pwd` | Display present working directory (where you are)
| `cd <path>` | Change Directory (`cd ..` will move up one directory. `cd ~` take you home)
| `ls -a` | List files in working directory ( use -a flag to show hidden files)
| `df -h` | Show free disk space. (use -h to make it human readable)
| `cp <source> <dest>` | Copy files. Handy for backing up a file before editing.
| `tar -xvzf` | Extract tar and gz files. (use -xvzf to extract and decompress files)
| `ps -aux` | List running processes 
| `netstat -antp` | List network connections and processes
| `tail -n x <filename>` | show the last x lines of a file. Default of 10 lines.

Also, learning how to use `grep`, even at a basic level, will be very helpful.

For example, `sudo cat /var/log/auth.log | grep failed` will display the auth log and only show lines with the word "failed" in it. Try it, you'll likely see failed login ssh login attempts.

## Putty (SSH)
I use [Putty for SSH access](https://www.chiark.greenend.org.uk/~sgtatham/putty/ "Get Putty") from my Windows PC. There is a console available on most VPS providers, but they are often klunky and do not always support copy and paste. 

Copy and paste with Putty is all about the mouse:
*   Copy: Select text with left mouse button.
*   Paste: Right click in the terminal

Get to know the loggin features and how to save profiles. My log file names look something like this: `serverlog-&Y-&M-&D-&T.txt` This will automatically create a file with the date and time of the ssh session. Make sure to also select *Session Output -> Printable Output* in the logging options.

## Root Access
I personally don't think it's a good idea to run everything as root. There are those that say in some cases it does not matter. Exercise Linux best practice regarding user control, logins, and access. Or not, up to you. That's what I love about Linux: *It is all up to you.* 

* The first thing to do is change the root's default password. Choose a good password.

    `# password` - change the password for a logged in user.

* Then create a new user and give that user root (sudo) access. Try not to use common usernames, you will see why when you look at your access logs later.

    * `# adduser <username>` - You will be prompted for a password. Choose a good one.

    * `# adduser <username> sudo` - Then we add that user to the "Sudo" group.

* Stay logged in as root, and open a **new putty session**

    * Login in with ssh (Putty) with your new user.
    *  Try to do something with sudo:

        `$ sudo cat /var/log/auth.log` - display the authorization log file, see who logs in.

        If you can get in with your user and sudo works, it is time to change prevent root from logging in with SSH. *You can still log in with root from your VPS console.*

* Logout of the root session. `# exit`

**Stay logged in with your other user.**

### Lock root out of ssh sessions by editing the ssh's config file.
> Linux is all about files. Learn how to edit files properly.

* With the user you just created, type:

    `$ sudo nano /etc/ssh/sshd_conf`

* Find the lines 
    * `PermitRootLogin yes `
    * `X11Forwarding yes`

* Change the 'yes' to 'no'. Exit nano by hitting `Ctrl+X'` then `Y` to save your changes.

* `$ sudo service ssh restart` to restart your ssh server

* Try logging into as root again. It should fail.

## Firewall
### ufw 
The next thing to do is install a firewall. Linux makes this pretty easy using `ufw`. 
* Install ufw if not already:

    `$ sudo apt-get install ufw`

* Set defaul incoming and outgoing rules:

    `$ sudo ufw default deny incoming`


    `$ sudo ufw default allow outgoing`


    `$ sudo ufw allow ssh`

* Enable the firewall:

    `$ sudo ufw enable`

*Note, depending on what you are using the server for, you may need to allow other ports. e.g. `ufw allow 9999/tcp`* 



### fail2ban
Fail2ban does just that. Repeated failed attempts will cause the firewall to deny access.
* install fail2ban:

    `$ sudo apt-get install fail2ban`

* Configure fail2ban.local

    Here we copy the default config file, the .local file will override the .conf file, we will edit the .local file.

    `$ sudo cp /etc/fail2ban/fail2ban.conf /etc/fail2ban/fail2ban.local`

* Configure jail.local

    Same idea as the fail2ban.conf file, copy to a .local and configure that one.

    `$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local`


* Configure jail settings

    The following settings can be adjusted to your preference. It can be tricky to get the hang of it right away. It was for me.
    
    `$ sudo nano /etc/fail2ban/jail.local` - Open the jail.local file for editing.

   * `bantime`
        
        The amount of seconds an IP is banned. Set a negative number for permanent ban.

    * `findtime`

        The length of seconds between logout attempts.

    * `maxretry`

        The amount of attempts allowed within the find time.

    Exit nano by hitting `Ctrl+X'` then `Y` to save your changes.    

* Whilelist IPs (if desired) Note, the jail names are in your jail.local file.

     By default, only sshd is enabled.

    `$ sudo fail2ban-client set <jail> addignoreip <ip address>`  
    For example: `$sudo fail2ban-client set sshd addignoreip 8.8.8.8`

* Check fail2ban's logs for ban activity.

    `$ sudo cat /var/log/fail2ban.log`

    or

    `$ sudo fail2ban-client sstatus sshd`

    

**That's it. Now people trying to log into your server will have a bit of a more difficult time of it.**

You can take it further by:
* Setup SSH to use public key authentication instead of logins.
* Add 2 factor authentications.  
* Add email reporting to fail2ban and ufw.
* Install tripwire or similar intrusion detection devices.

**Your VPS provider may have extra tools or services available to help secure your server.**

